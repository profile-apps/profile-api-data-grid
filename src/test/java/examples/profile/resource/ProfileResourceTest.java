package examples.profile.resource;


import static com.jayway.restassured.RestAssured.get;
import static com.jayway.restassured.RestAssured.given;
import static com.jayway.restassured.RestAssured.delete;
import static org.hamcrest.Matchers.equalTo;
import org.infinispan.client.hotrod.RemoteCache;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;

import examples.profile.Application;
import examples.profile.domain.Profile;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = Application.class)
@TestPropertySource(value = { "classpath:application.properties" })
@SpringBootTest(webEnvironment = WebEnvironment.DEFINED_PORT)
public class ProfileResourceTest {

	int port = 8080;

	ObjectMapper mapper = new ObjectMapper();

	@MockBean(name = "profileCache")
	RemoteCache<String, String> profileCache;

	@Before
	public void setBaseUri() {
		RestAssured.port = port;
		RestAssured.baseURI = "http://localhost";


	}

	
	@Test
	public void getProfileExistsTest() throws Exception {

		// given
		Profile profile = mockProfile();
		String json = serialize(profile);
		
		Mockito.when(profileCache.get("1")).thenReturn(json);

		// when/then
		get("/api/v1/profiles/1")
			.then()
			.assertThat()
				.statusCode(200)
				.body("id", equalTo("1"))
				.body("email", equalTo("philip.j.fry@planetexpress.com"))
				.body("firstName", equalTo("Philip"))
				.body("lastName", equalTo("Fry"))
				.body("gender", equalTo("Male"))
				.body("ipAddress", equalTo("54.176.137.99"));
		
	}

	@Test
	public void getProfileNotFoundTest() throws Exception {

		// given

		Mockito.when(profileCache.get("1")).thenReturn(null);

		// when/then
		get("/api/v1/profiles/1")
			.then()
			.assertThat()
				.statusCode(404);
		
	}

	@Test
	public void postProfileCreatedTest() throws Exception {

		// given
		String requestBody = serialize(mockProfile());

		Mockito.when(profileCache.put(Mockito.anyString(), Mockito.anyString())).thenReturn(null);

		// when/then
		given()
			.body(requestBody)
			.contentType(ContentType.JSON)
			.post("/api/v1/profiles")
		.then()
			.statusCode(201)
			.body("id", equalTo("1"))
			.body("email", equalTo("philip.j.fry@planetexpress.com"))
			.body("firstName", equalTo("Philip"))
			.body("lastName", equalTo("Fry"))
			.body("gender", equalTo("Male"))
			.body("ipAddress", equalTo("54.176.137.99"));

	}

	@Test
	public void postProfileAlreadyExistsTest() throws Exception {

		// given
		String requestBody = serialize(mockProfile());

		Mockito.when(profileCache.get("1")).thenReturn(requestBody);
		Mockito.when(profileCache.put(Mockito.anyString(), Mockito.anyString())).thenReturn(null);

		// when/then
		given()
			.body(requestBody)
			.contentType(ContentType.JSON)
			.post("/api/v1/profiles")
		.then()
			.statusCode(409);

	}

	@Test
	public void putProfileUpdatedTest() throws Exception {

		// given
		String persistedProfile = serialize(mockProfile());
		Profile updatedProfile = mockProfile();
		updatedProfile.setGender("Female");
		String requestBody = serialize(updatedProfile);

		Mockito.when(profileCache.get("1")).thenReturn(persistedProfile);
		Mockito.when(profileCache.replace(Mockito.anyString(), Mockito.anyString())).thenReturn(null);

		// when/then
		given()
			.body(requestBody)
			.contentType(ContentType.JSON)
			.put("/api/v1/profiles/1")
		.then()
			.statusCode(200)
			.body("id", equalTo("1"))
			.body("email", equalTo("philip.j.fry@planetexpress.com"))
			.body("firstName", equalTo("Philip"))
			.body("lastName", equalTo("Fry"))
			.body("gender", equalTo("Female"))
			.body("ipAddress", equalTo("54.176.137.99"));

	}

	@Test
	public void putProfileDoesNotExistTest() throws Exception {

		// given
		Profile updatedProfile = mockProfile();
		updatedProfile.setGender("Female");
		String requestBody = serialize(updatedProfile);

		Mockito.when(profileCache.get("1")).thenReturn(null);

		// when/then
		given()
			.body(requestBody)
			.contentType(ContentType.JSON)
			.put("/api/v1/profiles/1")
		.then()
			.statusCode(404);

	}

	@Test
	public void deleteProfileSuccess() throws Exception {

		// given
		String persistedProfile = serialize(mockProfile());

		Mockito.when(profileCache.get("1")).thenReturn(persistedProfile);
		Mockito.when(profileCache.remove("1")).thenReturn(null);

		// when/then
		delete("/api/v1/profiles/1")
			.then()
			.statusCode(200)
			.body("id", equalTo("1"))
			.body("email", equalTo("philip.j.fry@planetexpress.com"))
			.body("firstName", equalTo("Philip"))
			.body("lastName", equalTo("Fry"))
			.body("gender", equalTo("Male"))
			.body("ipAddress", equalTo("54.176.137.99"));

	}


	@Test
	public void deleteProfileDoesNotExist() throws Exception {

		// given

		Mockito.when(profileCache.get("1")).thenReturn(null);

		// when/then
		delete("/api/v1/profiles/1")
			.then()
			.statusCode(404);

	}

	private Profile mockProfile() {

		Profile p = new Profile();
		p.setId("1");
		p.setEmail("philip.j.fry@planetexpress.com");
		p.setFirstName("Philip");
		p.setLastName("Fry");
		p.setGender("Male");
		p.setIpAddress("54.176.137.99");

		return p;

	}

	private String serialize(Profile profile) throws Exception {
		return mapper.writeValueAsString(profile);
	}

}
