package examples.profile.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import examples.profile.domain.Profile;
import examples.profile.service.ProfileService;

@RestController
@RequestMapping("/api/v1")
public class ProfileResource {

	@Autowired
	private ProfileService service;

	@PostMapping("/profiles")
	@ResponseStatus(HttpStatus.CREATED)
	public Profile post(@RequestBody Profile profile) {
		return service.save(profile);
	}

	@GetMapping("/profiles/{id}")
	public Profile get(@PathVariable String id) {
		return service.get(id);
	}

	@PutMapping("/profiles/{id}")
	public Profile put(@PathVariable String id, @RequestBody Profile profile) {
		return service.update(id, profile);
	}

	@DeleteMapping("/profiles/{id}")
	public Profile delete(@PathVariable String id) {
		return service.delete(id);
	}

}
