package examples.profile.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import examples.profile.domain.Profile;
import examples.profile.exception.AlreadyExistsException;
import examples.profile.exception.NotFoundException;
import examples.profile.exception.SerializationException;
import examples.profile.repository.ProfileRepository;

@Service
public class ProfileService {

	@Autowired
	private ObjectMapper mapper;

	@Autowired
	private ProfileRepository repository;

	public Profile save(Profile profile) {

		String key = profile.getId();
		String value = serializeProfile(profile);

		try {
			get(key);
		} catch (Exception e) {

			// persist
			repository.put(key, value);

			return profile;

		}

		throw new AlreadyExistsException("value already exists using key '" + key + "'");

	}

	public Profile get(String key) {

		String value = repository.get(key);

		if (null == value) {
			throw new NotFoundException("no value in cache for key '" + key + "'");
		}

		return deserializeProfile(value);

	}

	public Profile update(String id, Profile profile) {

		// throws exception if not found
		get(id);

		// set id on object to be updated
		profile.setId(id);

		// serialize to json
		String value = serializeProfile(profile);

		// persist
		repository.replace(id, value);

		return profile;

	}

	public Profile delete(String key) {

		// get if it exists
		Profile profile = get(key);

		// remove from cache
		repository.remove(key);

		return profile;

	}

	private String serializeProfile(Profile profile) {

		try {
			return mapper.writeValueAsString(profile);
		} catch (JsonProcessingException e) {
			throw new SerializationException("failed to write profile to json string.");
		}

	}

	private Profile deserializeProfile(String json) {

		try {
			return mapper.readValue(json, Profile.class);
		} catch (Exception e) {
			throw new SerializationException("failed to deserialization json to profile.");
		}

	}

}
