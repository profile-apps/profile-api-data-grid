package examples.profile.config;

import org.infinispan.client.hotrod.RemoteCache;
import org.infinispan.client.hotrod.RemoteCacheManager;
import org.infinispan.commons.configuration.XMLStringConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ProfileCacheConfig {

	private static final String CACHE_CONFIG = "<infinispan><cache-container>"
			+ "<distributed-cache name=\"%s\"></distributed-cache>" + "</cache-container></infinispan>";

	@Bean(name = "profileCache")
	public RemoteCache<String, String> profileCache(RemoteCacheManager cacheManager) {

		RemoteCache<String, String> cache = cacheManager.administration().getOrCreateCache("profileCache", new XMLStringConfiguration(String.format(CACHE_CONFIG, "profileCache")));
		return cache;

	}

}
