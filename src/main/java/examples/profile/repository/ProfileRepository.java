package examples.profile.repository;

import org.infinispan.client.hotrod.RemoteCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

@Repository
public class ProfileRepository {

	@Autowired
	@Qualifier("profileCache")
	private RemoteCache<String, String> profileCache;

	public String put(String key, String value) {		
		return profileCache.put(key, value);
	}

	public String get(String key) {
		return profileCache.get(key);
	}

	public String replace(String key, String value) {
		return profileCache.replace(key, value);
	}

	public String remove(String key) {
		return profileCache.remove(key);
	}

}
