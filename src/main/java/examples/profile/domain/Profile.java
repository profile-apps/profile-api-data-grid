package examples.profile.domain;

import lombok.Data;

@Data
public class Profile {

	private String id;
	private String firstName;
	private String lastName;
	private String email;
	private String gender;
	private String ipAddress;

}
